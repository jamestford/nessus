#!/bin/bash

#### CONFIGURATION

# Keep backups which are XX days old or less.
DAYSOLD=30

# Administrative email where notifications will be sent.
ADMINEMAIL=james.t.ford@gmail.com

# This is the base path for backups. This could be a NFS share, local storage,
# a backup LUN, etc.
BACKUP_PATH=/opt/backups

# Whats the maximum amount of time that we want to wait before timing out the
# backup?
TIMEOUT=1800

#### DO NOT EDIT BELOW THIS LINE

## Backup Security Center Data
if [[ ! -e $BACKUP_PATH ]]; then
    echo "Creating backup path at $BACKUP_PATH"
    mkdir -p $BACKUP_PATH
else
    echo "Using pre-existing backup path at $BACKUP_PATH"
fi

echo "Trimming previous backups ..."
find $BACKUP_PATH -mtime +$DAYSOLD -exec rm {} \;

## Shutdown Function
#
# This function will shudown SecurityCenter and will not return back until all
# SecurityCenter related processes are completed. If we end up having to wait
# past the TIMEOUT value, then it will drop out as well.
function shutdown_securitycenter()
{
        local is_running=1 # True
        local start_time=$(date +%s)
        local tns_process_count=1
        service SecurityCenter stop
        while [ $is_running -eq 1 ]; do
        tns_process_count=$(set -o pipefail; ps -U tns --no-headers | wc -l)
                if [ ${tns_process_count:-1} -eq 0 ]; then
                        is_running=0 # False
                else
                        sleep 1
                        if [ $(( $(date +%s) - $start_time )) -gt $TIMEOUT ]; then
                                is_running=2 # Timeout
                        fi
                fi
        done
        return $is_running
}

## Backup Generator
#
# Here is where we will actually perform the backup. The tarball that we
# generate will ONLY contain SecurityCenter data, not the binaries, scripts,
# or code that is installed along with SecurityCenter. This makes the data more
# portable in the end as its no longer dependent on architecture, simply just
# the version of SC that it was backed up from.
function backup_securitycenter()
{
        local rc
        local sc_version=$(rpm -q --qf '%{v}' SecurityCenter)
        local bdate=$(date +%Y-%m-%d)
        local tarball="${BACKUP_PATH}/sc-backup-${bdate}.${sc_version}.tar.gz"
        local -a bfiles
        bfiles=(
                /opt/sc4/admin
                /opt/sc4/data
                /opt/sc4/orgs
                /opt/sc4/repositories
                /opt/sc4/*db
        )

        tar -zcf "$tarball" "${bfiles[@]}"
        rc=$?
        if [ $rc -ne 0 ]; then
                mv $tarball "${tarball/sc-backup-/sc-backup-errors-}"
        fi

        return $rc
}

## Main Loop
#
# Now lets actually perform the backup. If there is an error with shutting
# everything down, then print out the processes that are still running. Lastly,
# start everything back up.

if shutdown_securitycenter; then
        if ! backup_securitycenter; then
                echo 'CRITICAL: Backup had errors.'
        fi
else
        echo 'CRITICAL: Could not Shutdown SecurityCenter within specified timeout.'
        echo 'CRITICAL: Processes Still Running:'
        ps fU tns
fi

service SecurityCenter start

## Email Administrator
/bin/mail -s "Security Center Backup has finished!" "$ADMINEMAIL" < /dev/null
